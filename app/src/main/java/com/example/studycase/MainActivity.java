package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText input1,input2;
    TextView output;
    Button hasilBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input1 = (EditText) findViewById(R.id.inputpertama);
        input2 = (EditText) findViewById(R.id.inputkedua);
        output = (TextView) findViewById(R.id.hasil);
        hasilBtn = findViewById(R.id.button);

        hasilBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                hasil();
            }
        });
    }

    private void hasil() {
        double panjang = Double.parseDouble(input1.getText().toString());
        double lebar = Double.parseDouble(input2.getText().toString());
        double luas = panjang * lebar;
        output.setText(Double.toString(luas));
    }
}
